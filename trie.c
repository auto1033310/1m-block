#include "trie.h"

struct Trie* getNewTrieNode()
{
    struct Trie* node = (struct Trie*)malloc(sizeof(struct Trie));
    node->isLeaf = 0;
 
    for (int i = 0; i < CHAR_SIZE; i++) {
        node->character[i] = NULL;
    }
 
    return node;
}

void insert(struct Trie *head, char* str)
{
    
    struct Trie* curr = head;
    while (*str)
    {
        
        if (curr->character[*str - '!'] == NULL) {
            curr->character[*str - '!'] = getNewTrieNode();

        }
 
        
        curr = curr->character[*str - '!'];
 
        
        str++;
    }
 
    
    curr->isLeaf = 1;
}

int search(struct Trie* head, char* str)
{
   
    if (head == NULL) {
        return 0;
    }
    time_t search_start=time(NULL);
    struct Trie* curr = head;
    while (*str)
    {
        
        curr = curr->character[*str - '!'];
 
        
        if (curr == NULL) {
            return 0;
        }
 
        
        str++;
    }
    time_t search_end=time(NULL);
    printf("search time: %f\n",(float)(search_end-search_start));
    return curr->isLeaf;
}

int hasChildren(struct Trie* curr)
{
    for (int i = 0; i < CHAR_SIZE; i++)
    {
        if (curr->character[i]) {
            return 1;       
        }
    }
 
    return 0;
}
int deletion(struct Trie **curr, char* str)
{
    
    if (*curr == NULL) {
        return 0;
    }
 
    
    if (*str)
    {
        
        if (*curr != NULL && (*curr)->character[*str - '!'] != NULL &&
            deletion(&((*curr)->character[*str - '!']), str + 1) &&
            (*curr)->isLeaf == 0)
        {
            if (!hasChildren(*curr))
            {
                free(*curr);
                (*curr) = NULL;
                return 1;
            }
            else {
                return 0;
            }
        }
    }
 
    
    if (*str == '\0' && (*curr)->isLeaf)
    {
        
        if (!hasChildren(*curr))
        {
            free(*curr);    
            (*curr) = NULL;
            return 1;       
        }
 
        
        else {
            
            (*curr)->isLeaf = 0;
            return 0;       
        }
    }
 
    return 0;
}
