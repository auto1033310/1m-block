nqueue=-lnetfilter_queue

all: 1m-block

1m-block.o: 1m-block.c trie.h

trie.o: trie.c trie.h

1m-block: 1m-block.o trie.o
	$(LINK.cc) $^ $(nqueue) -o $@

clean:
	rm -f *.o 1m-block