
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <libnet.h>
#include "trie.h"

#include <libnetfilter_queue/libnetfilter_queue.h>

#define ipv4_type 0x800
#define TCP_protocol 0x6
#define HTTP_dp 80

#define true 1
#define false 0

struct Trie* malicious_trie_head;

void dump(unsigned char* buf, int size) {
    int i;
    printf("\n");
    for (i = 0; i < size; i++) {
        if (i != 0 && i % 16 == 0)
            printf("\n");
        printf("%02X ", buf[i]);
    }
    printf("\n");
}


/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
    int id = 0;
    struct nfqnl_msg_packet_hdr *ph;
    struct nfqnl_msg_packet_hw *hwph;
    u_int32_t mark,ifi;
    int ret;
    unsigned char *data;

    ph = nfq_get_msg_packet_hdr(tb);
    if (ph) {
        id = ntohl(ph->packet_id);
        printf("hw_protocol=0x%04x hook=%u id=%u ",
            ntohs(ph->hw_protocol), ph->hook, id);
    }

    hwph = nfq_get_packet_hw(tb);
    if (hwph) {
        int i, hlen = ntohs(hwph->hw_addrlen);

        printf("hw_src_addr=");
        for (i = 0; i < hlen-1; i++)
            printf("%02x:", hwph->hw_addr[i]);
        printf("%02x ", hwph->hw_addr[hlen-1]);
    }

    mark = nfq_get_nfmark(tb);
    if (mark)
        printf("mark=%u ", mark);

    ifi = nfq_get_indev(tb);
    if (ifi)
        printf("indev=%u ", ifi);

    ifi = nfq_get_outdev(tb);
    if (ifi)
        printf("outdev=%u ", ifi);
    ifi = nfq_get_physindev(tb);
    if (ifi)
        printf("physindev=%u ", ifi);

    ifi = nfq_get_physoutdev(tb);
    if (ifi)
        printf("physoutdev=%u ", ifi);

    ret = nfq_get_payload(tb, &data);
    if (ret >= 0){
        //dump(data, ret);
        printf("payload_len=%d\n", ret);
    }
    fputc('\n', stdout);

    return id;
}

static int parse_host(char* host, char* http_hdr, int size){
    char* host_pos;
    int i, j;
    for(i=0;i<size;i++){
        if(!strncmp(http_hdr+i, "Host: ",6)){
            host_pos = (char*)(http_hdr+i+6);
            for(j=0;j<size-i;j++){
                if(host_pos[j]==0x0d) {
                    host[j]=0;
                    break;
                }
                host[j]=host_pos[j];


            }
            return true;
        }
    }
    return false;
}

static int check_if_censored(unsigned char *packet, int size){
    struct libnet_ethernet_hdr *e;
    struct libnet_ipv4_hdr* I;
    struct libnet_tcp_hdr* T;
    char* http_hdr;
    char host[200]={0,};
    int http_pos;

    I= (struct libnet_ipv4_hdr*)(packet);

    if(I->ip_p!=TCP_protocol) return false;

    T= (struct libnet_tcp_hdr*)(packet+4*I->ip_hl);

    if(ntohs(T->th_dport)!=HTTP_dp) return false;

    http_pos = 4*I->ip_hl+sizeof(*T);

    http_hdr = (char*)(packet+http_pos);

    if(!parse_host(host, http_hdr, size-http_pos)) return false;

    return search(malicious_trie_head, host) ? true : false;
}

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
          struct nfq_data *nfa, void *data)
{
    u_int32_t id = print_pkt(nfa);
    int ret;
    unsigned char *payload;


    ret=nfq_get_payload(nfa, &payload);
    if(ret>=0){
        if(check_if_censored(payload, ret)){

            printf("harmful site detected!\ndropped\n");

            return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
        }
    }




    //printf("entering callback\n");


    return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

int main(int argc, char **argv)
{
    struct nfq_handle *h;
    struct nfq_q_handle *qh;
    struct nfnl_handle *nh;
    int fd;
    int rv;
    char buf[4096] __attribute__ ((aligned));
    char line[1000];
    char *host_str;
    int i=0;

    malicious_trie_head=getNewTrieNode();

    FILE *pFILE =NULL;
    pFILE = fopen("top-1m.csv", "r");

    time_t learn_start= time(NULL);

    while(!feof(pFILE)){
        i++;
        fgets(line, 999, pFILE);
        strtok(line, ",");
        host_str=strtok(NULL, "\n");

        insert(malicious_trie_head, host_str);

    }
    time_t learn_end = time(NULL);

    printf("learn time: %f\n",(float)(learn_end-learn_start));
    fclose(pFILE);

    //printf("opening library handle\n");
    h = nfq_open();
    if (!h) {
        fprintf(stderr, "error during nfq_open()\n");
        exit(1);
    }

    //printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
    if (nfq_unbind_pf(h, AF_INET) < 0) {
        fprintf(stderr, "error during nfq_unbind_pf()\n");
        exit(1);
    }

    //printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
    if (nfq_bind_pf(h, AF_INET) < 0) {
        fprintf(stderr, "error during nfq_bind_pf()\n");
        exit(1);
    }

    //printf("binding this socket to queue '0'\n");
    qh = nfq_create_queue(h,  0, &cb, argv);
    if (!qh) {
        fprintf(stderr, "error during nfq_create_queue()\n");
        exit(1);
    }

    //printf("setting copy_packet mode\n");
    if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
        fprintf(stderr, "can't set packet_copy mode\n");
        exit(1);
    }

    fd = nfq_fd(h);

    for (;;) {
        if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
            printf("pkt received\n");
            nfq_handle_packet(h, buf, rv);
            continue;
        }
        /* if your application is too slow to digest the packets that
         * are sent from kernel-space, the socket buffer that we use
         * to enqueue packets may fill up returning ENOBUFS. Depending
         * on your application, this error may be ignored. nfq_nlmsg_verdict_putPlease, see
         * the doxygen documentation of this library on how to improve
         * this situation.
         */
        if (rv < 0 && errno == ENOBUFS) {
            printf("losing packets!\n");
            continue;
        }
        perror("recv failed");
        break;
    }

    printf("unbinding from queue 0\n");
    nfq_destroy_queue(qh);

#ifdef INSANE
    /* normally, applications SHOULD NOT issue this command, since
     * it detaches other programs/sockets from AF_INET, too ! */
    printf("unbinding from AF_INET\n");
    nfq_unbind_pf(h, AF_INET);
#endif

    printf("closing library handle\n");
    nfq_close(h);

    exit(0);
}
