#ifndef TRIE_H
#define TRIE_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CHAR_SIZE 94

struct Trie
{
    int isLeaf;             
    struct Trie* character[CHAR_SIZE];
};

//unsigned long long used_size;

struct Trie* getNewTrieNode();
void insert(struct Trie *head, char* str);
int search(struct Trie* head, char* str);
int hasChildren(struct Trie* curr);
int deletion(struct Trie **curr, char* str);

#endif // TRIE_H
